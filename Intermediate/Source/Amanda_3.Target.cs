using UnrealBuildTool;

public class Amanda_3Target : TargetRules
{
	public Amanda_3Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Amanda_3");
	}
}
