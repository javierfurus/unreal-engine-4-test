// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OVRLipSync/Public/OVRLipSyncFrame.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOVRLipSyncFrame() {}
// Cross Module References
	OVRLIPSYNC_API UScriptStruct* Z_Construct_UScriptStruct_FOVRLipSyncFrame();
	UPackage* Z_Construct_UPackage__Script_OVRLipSync();
	OVRLIPSYNC_API UClass* Z_Construct_UClass_UOVRLipSyncFrameSequence_NoRegister();
	OVRLIPSYNC_API UClass* Z_Construct_UClass_UOVRLipSyncFrameSequence();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FOVRLipSyncFrame::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OVRLIPSYNC_API uint32 Get_Z_Construct_UScriptStruct_FOVRLipSyncFrame_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOVRLipSyncFrame, Z_Construct_UPackage__Script_OVRLipSync(), TEXT("OVRLipSyncFrame"), sizeof(FOVRLipSyncFrame), Get_Z_Construct_UScriptStruct_FOVRLipSyncFrame_Hash());
	}
	return Singleton;
}
template<> OVRLIPSYNC_API UScriptStruct* StaticStruct<FOVRLipSyncFrame>()
{
	return FOVRLipSyncFrame::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOVRLipSyncFrame(FOVRLipSyncFrame::StaticStruct, TEXT("/Script/OVRLipSync"), TEXT("OVRLipSyncFrame"), false, nullptr, nullptr);
static struct FScriptStruct_OVRLipSync_StaticRegisterNativesFOVRLipSyncFrame
{
	FScriptStruct_OVRLipSync_StaticRegisterNativesFOVRLipSyncFrame()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OVRLipSyncFrame")),new UScriptStruct::TCppStructOps<FOVRLipSyncFrame>);
	}
} ScriptStruct_OVRLipSync_StaticRegisterNativesFOVRLipSyncFrame;
	struct Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VisemeScores_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisemeScores_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VisemeScores;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaughterScore_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LaughterScore;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OVRLipSyncFrame.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOVRLipSyncFrame>();
	}
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores_Inner = { "VisemeScores", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores_MetaData[] = {
		{ "ModuleRelativePath", "Public/OVRLipSyncFrame.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores = { "VisemeScores", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOVRLipSyncFrame, VisemeScores), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_LaughterScore_MetaData[] = {
		{ "ModuleRelativePath", "Public/OVRLipSyncFrame.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_LaughterScore = { "LaughterScore", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOVRLipSyncFrame, LaughterScore), METADATA_PARAMS(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_LaughterScore_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_LaughterScore_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_VisemeScores,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::NewProp_LaughterScore,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OVRLipSync,
		nullptr,
		&NewStructOps,
		"OVRLipSyncFrame",
		sizeof(FOVRLipSyncFrame),
		alignof(FOVRLipSyncFrame),
		Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOVRLipSyncFrame()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOVRLipSyncFrame_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OVRLipSync();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OVRLipSyncFrame"), sizeof(FOVRLipSyncFrame), Get_Z_Construct_UScriptStruct_FOVRLipSyncFrame_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOVRLipSyncFrame_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOVRLipSyncFrame_Hash() { return 797736604U; }
	void UOVRLipSyncFrameSequence::StaticRegisterNativesUOVRLipSyncFrameSequence()
	{
	}
	UClass* Z_Construct_UClass_UOVRLipSyncFrameSequence_NoRegister()
	{
		return UOVRLipSyncFrameSequence::StaticClass();
	}
	struct Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrameSequence_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FrameSequence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OVRLipSync,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "OVRLipSyncFrame.h" },
		{ "ModuleRelativePath", "Public/OVRLipSyncFrame.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence_Inner = { "FrameSequence", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOVRLipSyncFrame, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence_MetaData[] = {
		{ "ModuleRelativePath", "Public/OVRLipSyncFrame.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence = { "FrameSequence", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOVRLipSyncFrameSequence, FrameSequence), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::NewProp_FrameSequence,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOVRLipSyncFrameSequence>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::ClassParams = {
		&UOVRLipSyncFrameSequence::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOVRLipSyncFrameSequence()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOVRLipSyncFrameSequence_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOVRLipSyncFrameSequence, 2065746738);
	template<> OVRLIPSYNC_API UClass* StaticClass<UOVRLipSyncFrameSequence>()
	{
		return UOVRLipSyncFrameSequence::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOVRLipSyncFrameSequence(Z_Construct_UClass_UOVRLipSyncFrameSequence, &UOVRLipSyncFrameSequence::StaticClass, TEXT("/Script/OVRLipSync"), TEXT("UOVRLipSyncFrameSequence"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOVRLipSyncFrameSequence);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
